const fs = require('fs')
const path = require('path')
const { RawSource, ReplaceSource } = require('webpack-sources')
const app = require('../package.json')

class PwaPlugin {
  /**
   * Set up the plugin.
   * @param {object} options - the plugin's options
   */
  constructor (options = {}) {
    // TODO: these should come from the options object
    //  (which should be validated and have defaults)

    // set up service worker options
    this.sw = {
      // FIXME: make this more sophisticated (array of paths? eg: ['**/*.map', ...])
      //  also make sure we're in-/ex-cluding the right things (robots.txt, app.manifest)
      exclude: /\.map$/
    }

    // set up application manifest options
    const name = app.name
      .split(/[-_]/)
      .map(w => w[0].toUpperCase() + w.slice(1))
      .join(' ')
    this.pwa = {
      name: name,
      short_name: name,
      display: 'fullscreen',
      start: '/index.html',
      bg: '#fafaff',
      theme: '#e6b451',
    }
  }

  /**
   * Apply the plugin.
   * @param {object} compiler - the webpack compiler
   */
  apply (compiler) {
    compiler.hooks.emit.tap('PwaPlugin', compilation => {
      // if env is not production don't register the service worker
      if (process.env.NODE_ENV === 'production') {
        // get template file contents and inject assets
        const assets = compilation.getAssets()
          .filter(asset => !this.sw.exclude.test(asset.name))
          .map(asset => `'${asset.name}'`)
        const sw = fs.readFileSync(path.join(__dirname, 'sw.js'))
          .toString()
          .replace(/'%{assets}'/, `[${assets.join(',')}]`)
        // emit new service worker
        compilation.emitAsset('sw.js', new RawSource(sw))
      }

      // get relevant settings & build out application manifest
      const images = compilation.getAssets()
        .filter(asset => asset.name.startsWith('icons/')) // FIXME: this defs could be more robust
        .map(image => {
          const icon = { src: image.name }
          const match = image.name.match(/-(?<size>\d+x\d+)\.[a-z]{3,4}/)
          if (match) icon.sizes = match.groups.size
          return icon
        })
      const manifest = JSON.stringify({
        name: app.name,
        short_name: app.name,
        start_url: this.pwa.start,
        display: this.pwa.display,
        background_color: this.pwa.bg,
        // TODO: can we expose this to the html plugin, for use in index.html (in the meta theme-color tag)?
        theme_color: this.pwa.theme,
        icons: images
      })
      // emit application manifest
      compilation.emitAsset('app.manifest', new RawSource(manifest))

      // inject relevant tags in html head
      compilation.updateAsset('index.html', source => {
        const newSource = new ReplaceSource(source)
        newSource.insert(
          source.source().indexOf('<title>'),
          `<meta name="theme-color" content="${this.pwa.theme}">`
        )
        return new RawSource(newSource.source())
      })
    })
  }
}

module.exports = PwaPlugin
