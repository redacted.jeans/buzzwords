self.addEventListener('install', function (event) {
  event.waitUntil(
    caches.open('buzzword-assets').then(function (cache) {
      return cache.addAll('%{assets}')
    })
  )
})

self.addEventListener('fetch', function (event) {
  console.log(event.request.url)
  event.respondWith(
    caches.match(event.request).then(function (response) {
      return response || fetch(event.request)
    })
  )
})
