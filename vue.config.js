const PwaPlugin = require('./plugins/pwa-plugin')

module.exports = {
  configureWebpack: {
    plugins: [
      new PwaPlugin()
    ]
  },
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = args[0].title
          .split(/[-_]/)
          .map(w => w[0].toUpperCase() + w.slice(1))
          .join(' ')
        return args
      })
  }
}
