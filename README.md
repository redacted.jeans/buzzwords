# Buzzwords
[![Netlify Status](https://api.netlify.com/api/v1/badges/1cc0f776-7d71-4599-9262-fb41b5208980/deploy-status)](https://app.netlify.com/sites/buzzwords/deploys)

This project is a clone of the [New York Time's Spelling Bee game](https://www.nytimes.com/puzzles/spelling-bee) built in Vue.

# Words
The word list is currently generated from my laptop's (running Ubuntu 20.04 Focal Fossa) `/usr/share/dict/words` file. The script found in `words/gen.js` grabs that file and filters down the words according to the following rules:
1. word must be 4+ letters long;
2. word must not be a proper noun (cannot contain any uppercase letters);
3. word must not be a compound word (cannot contain a hyphen or apostrophe); and
4. word must not have more than 7 unique characters.

These rules filter down the `words` file from 102,402 to 41,024 words, 14,108 of which are valid pangrams.
