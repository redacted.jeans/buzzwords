const fs = require('fs')
const path = require('path')
const t = {
  BRIGHT: '\x1b[1m',
  END: '\x1b[0m'
}

// get letters & print them
if (process.argv.length < 3) throw Error('Must include letters to solve\n    usage: node ./solve.js honeybs')
const letters = process.argv[2].split('').filter((c, i, w) => w.indexOf(c) === i)
if (letters.length < 7) throw Error(`Too few unique letters (${letters.length})`)
if (letters.length > 7) throw Error(`Too many unique letters (${letters.length})`)
console.log(`solving for ${t.BRIGHT}${letters[0]}${t.END},${letters.slice(1)}`)

// load the previously generated word list
fs.readFile(path.join(__dirname, '../public/words.txt'), 'utf8', (err, file) => {
  if (err) throw err

  // filter out words according to the following rules:
  //  1. words must be 4+ letters long
  //  2. words must not contain uppercase characters (no proper nouns)
  //  3. words must not contain hyphens or apostrophes (no compound words)
  //  4. words must not have more than 7 unique characters
  const words = file.split(/\r?\n/).filter(word => {
    return (word.length > 3) &&
      (word === word.toLowerCase()) &&
      (!word.includes('-') && !word.includes("'")) &&
      ([...word].filter((c, i, w) => w.indexOf(c) === i).length <= 7)
  })

  // filter out all words that don't include the center letter of that include letters not in the list
  const valid = words.filter(word => {
    return word.includes(letters[0]) &&
      [...word].filter(char => !letters.includes(char)).length === 0
  })
  // calculate the max score
  const score = valid.reduce((acc, word) => {
    const base = (word.length === 4) ? 1 : word.length
    const pan = ([...word].filter((c, i, w) => w.indexOf(c) === i).length === 7) ? 7 : 0
    return acc + base + pan
  }, 0)

  // print results
  console.log(valid, valid.length, score)
})
