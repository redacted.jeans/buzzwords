const fs = require('fs')
const path = require('path')

// get file location
const resolved = path.resolve(process.argv[2] || '/usr/share/dict/words')
console.log(`loading ${resolved}`)

// load the system's words file
fs.readFile(resolved, 'utf8', (err, file) => {
  if (err) throw err

  // filter out words according to the following rules:
  //  1. words must be 4+ letters long
  //  2. words must not contain uppercase characters (no proper nouns)
  //  3. words must not contain hyphens or apostrophes (no compound words)
  //  4. words must not have more than 7 unique characters
  const words = file.split(/\r?\n/).filter(word => {
    return (word.length > 3) &&
      (word === word.toLowerCase()) &&
      (!word.includes('-') && !word.includes("'")) &&
      ([...word].filter((c, i, w) => w.indexOf(c) === i).length <= 7)
  })

  // output stats
  console.log('total\t\tfiltered\tpangrams')
  const total = file.split(/\r?\n/).length
  const filtered = words.length
  const pangrams = words.filter(word => {
    return [...word].filter((c, i, w) => w.indexOf(c) === i).length === 7
  }).length
  console.log(`${total.toLocaleString()}\t\t${filtered.toLocaleString()}\t\t${pangrams.toLocaleString()}`)

  // write the words to a new file in the src/assets directory
  fs.writeFile(path.join(__dirname, '../public/words.txt'), words.join('\n'), { flag: 'w' }, err => {
    if (err) throw err
  })
})
