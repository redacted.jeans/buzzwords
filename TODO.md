TODO
===

Init
---
- better word list: ENABLE2k, YAWL
- normalize words before filtering (i.e. get rid of diacritics: épée > epee)
  - has to be before so diacritics aren't counted agains unique char count
  - both enable1 and yawl have already done this
- enforce further constraints on randomly selected pangram
  1. never choose a pangram with only one vowel
  2. never choose a pangram with more than three/four vowels
  3. never choose a pangram + center letter resulting in too few words (less than 20?)
  4. never choose a pangram + center letter resulting in too many words (more than 200?)
  - it's possible nos 3 & 4 together are enough to disregard nos 1 & 2
  - in that case, maybe the range should be user-selectable (with some reasonable defaults)
- when loading from hash, validate the hash first and gracefully handle if not

Core Functionality
---
- move state stuff into a module maybe?
- add cache-/localStorage-busting mechanism to word list in case it changes
  - word list should live in webworker/pwa stuff
- upgrade to vue3
- ... tests

UI/UX
---
- alerts should look different based on contents
  - light for success, dark for already found/not in list, accent for buzzword
- add something (animation? alert?) when user reaches various stages (esp. genius & qb)
- letters that aren't in current game should show up as faded when entered
  - technically those letters aren't possible to enter
  - might still be a nice condition in case it ever happens
- if any words have been found, re-initialising should get confirmation before deleting game progress
- automatically close word list when user enters a new letter/word
- CSS or stylus var:
  - z-index (stylus) (bottom, middle, top?)
  - transition/animation speeds (fast, medium, slow?)
  - better media queries/responsive styling
- use semantic html everywhere
  - where possible use semantic tag (e.g. progress bar: <progress></progress>)
  - add aria-roles where non-semantic tags are used
- use [rscss](https://rscss.io/) as css naming convention?
  - not clear this is helpful within a (scoped) vue single-file component
- better loading:
  - should have some default non-vue loading element inside #app that gets replaced once vue is mounted
  - look into inlining some css and/or js in index.html (webpack loader?)
- make it look,,, not shite
  - new theme!
  - skeleton UI animation
  - light & dark modes (based on os settings, user-changeable)
    - dark mode exists but is bad
  - figure out transitions/animations
    - alert show/hide
    - word list open/close
    - buttons?
  - at small screen size, change word font-size when too long
  - add two-column layout for big screens
- fix bee favicon in dark mode
- let user enter letters by typing (+ backspace removes, enter submits, space shuffles)
- allow sorting word list alphabetically, by score, & by order found

Under Consideration
---
- provide definition of word in word list
- let people choose from various word lists
  - support other languages
  - custom word lists?
- seed based on day (so a new round every day)
  - implement a history so people can play any day in the past
- allow custom/explicit seeds
- various gamification features
  - two-player/competitive mode
  - leaderboards
  - streaks
  - stats
  - achievements
